+++
title = "Project Ideas"
weight = 50
description = "Project ideas"
bref= "For understanding the ideas behind Flowee, and for people that want to help out"
singlepage = true
toc = true
+++

Flowee ships a Hub as well as an assortment of smaller tools. The goal of
Flowee is to make it easy for people to create new projects using the
platform that Flowee the Hub provides.

In an ecosystem of products, the value of the whole thing goes up as more projects are added.
New products can be combined with existing ones and should it make sense the
new products provide an interface for other Flowee apps to use.


Here is a list of example projects that would be nice to add to the Flowee
family.

### Mining service

Mining in Bitcoin Cash (and Bitcoin) happens currently by using a full node that regularly gets an RPC call 'getblocktemplate', which is then parsed by the mining software and modified according to miner-specific wishes before being offered to the actual mining infrastructure.

There are quite a number of problems in today's approach, most of them are hardly relevant for small blocks, but as blocks grow these issues become actual problems – stoppers to the growth.

In Flowee we have a lot of infrastructure in place that will be helpful in constructing a future-ready solution.

We would need a new application, a service. This service will run close to a Flowee The Hub service and be in close contact with it via the Flowee APIs. This chaining means that the mining service will get all its data after it has  already been validated against network rules. Blocks, block-headers, and transactions do not need to be validated for correctness again.

The mining service application will have its own mempool. It will get all the transactions from the Hub and at a quick glance you might say it will just duplicate the mempool from the Hub.

The Bitcoin Core view of mempools is that it is best to keep them as similar to each other as possible, as this helps to speed up the block-propagation. The assumption is that if your mempool is as similar as possible to the one the miner has then transferring the block is practically free.

Naturally, this assumption means that anyone not following this rule instantly has an advantage over other miners. Which means that eventually miners will no longer be able to depend on it. The when is just a matter of time. As block-rewards fall, it will start to be beneficial to have as many transactions in your validation-mempool as possible. The more transactions you received and validated before the new block comes in, the better.

At one point the illusion that mempool policies will stay homogeneous across the network shatters. A miner with a deeper accept-depth will have more fee-paying transactions they can mine and gain more money. So when fees start to become a bigger part (and block-rewards go down) this will become a competitive game.

When the mempool you maintain in your validating node no longer represents the mempool of a remote miner, then it makes sense to loosen the policy. Have more transactions in your mempool and your validation time will go down.

For this to work you need to de-couple your validation-mempool from your mining-mempool.

A validation mempool doesn't care about things like child-pays-for-parent. It makes it possible to remove any technical limitations on chain-depth. Similarly the validation-mempool doesn't need to sort transactions on fees, which really makes the full node able to process them at a higher pace. This makes transaction acceptance much more parallel than it already is. This is a core scaling factor.

Then the mempool in the mining service will be quite different. Its acceptance of transactions includes recalculations based on things like child-pays-for-parent. But doing these re-calculations can be done across multiple processors. Only when one is done do the transactions affected have to be updated.

The miner transaction is used to create a block-template. That is its main purpose. The best way to do this is to continuously update the block-template as new transactions come in. Any request for the block template will just take the most recently known one allowing the request to return instantly.

The mining service project will then talk to the Hub API on one side and use Stratum on the other side to communicate with existing mining infrastructure.

In general this mining service project  separates the mining mempool from the validation mempool, which makes both validation and mining more effective and easier to maintain.

I expect this to bring mining waiting-times down to milliseconds level while allowing block sizes much greater than what we have today.

### Config / Control app

The idea is that this app can connect to unconfigured instances of Flowee
apps and duplicate config, duplicate blockchain and it can allow you to
direct the log messages into a shared database you can run queries on to
find issues like if one node is substantially slower than others.

### Api-As-a-Service (AaaS)

Running Flowee the Hub may be something that individuals or companies can
do on their own hardware and this component would supply the underlying API
to paying users on the internet.

Users of the Flowee hub can access most blockchain information
but only when they have an actual account. An account may be done in many
ways, for instance a monthly subscription or a pay-as-you-go where the amount
of queries is counted until you are out of coins.
Payment-channels are also an interesting idea.

In short, a proxy on top of Flowee the Hub where people get paid to run a
full node by providing the services they provide to paying users.

### Statistics module

A way to get data without waiting for a 3rd party server to generate it.

A module that talks to a Hub and generates statistical data of the sort we
now find on sites like fork.lol or blockchain.info would be the goal.
Ideally the app allows more than a pre-defined set of queries and opens up
the blockchain to academics and researchers that want to do more than see
the average fee.

### Specialized transaction creation. Like date-stamping

An app that has several specialized ways to create a transaction which can
be signed and sent to the network.

The most common usecase of special transactions is one where you take a
document (like a PDF) and the application creates a hash which is then
stored in an op-return type transaction. This provides time-stamping. A
proof on the blockchain that at the time of mining that document existed.
