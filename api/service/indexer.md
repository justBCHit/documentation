+++
title = "Service/Indexer"
toc= true
weight = 19
parent = "api"
+++

### Indexer Service

Indexer is a service provided by the server of the same name. It operates
on a different internet port (1234) and might run on a different IP address
than *the Hub*.

Read more in the [*Indexer* intro](/indexer/).

### Messages

#### GetAvailableIndexers,

<table class="api">
<tr><td colspan="3">Indexer. sId=19 mId=<code>0</code></td></tr>
</table>


#### GetAvailableIndexersReply

<table class="api">
<tr><td colspan="3">Indexer. sId=19 mId=<code>1</code></td></tr>
<tr><td>AddressIndexer: <code>21</code></td><td>boolean</td><td>included (true) if available</td></tr>
<tr><td>TxIdIndexer <code>22</code></td><td>boolean</td><td>included (true) if available</td></tr>
<tr><td>SpentOutputIndexer <code>23</code></td><td>boolean</td><td>included (true) if available</td></tr>
</table>

#### FindTransaction

You can only ask for a transaction if the txid indexer is enabled.

<table class="api">
<tr><td colspan="3">Indexer. sId=19 mId=<code>2</code></td></tr>
<tr><td>TxId: <code>4</code></td><td>bytearray</td><td>32-byte transaction hash</td></tr>
</table>

#### FindTransactionReply

<table class="api">
<tr><td colspan="3">Indexer. sId=19 mId=<code>3</code></td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>natural-number</td><td>Height of the block in the chain.</td></tr>
<tr><td>OffsetInBlock: <code>8</code></td><td>natural-number</td><td>Key
useful for BlockChain::GetTransaction</td></tr>
</table>

#### FindAddress

You can only ask about an address if the address indexer is enabled.

The indexer works on output-script-hash. The sha256 (single) hashing of the entire output script.
You can use the BitcoinScriptHashed (32-byte) flag for such.

Additionally we suppport a simple p2pkh based bitcoin address. This is the ripe160 hash of the public key, which is the type of addresses most payments are made to. The indexer will convert this to a script-hash for you. Use the BitcoinP2PKHAddress tag for this.

Most user-interaction uses some encoding, base58 or cash-address are two
examples. This API is not meant to be user-facing and we expect the
string-based address to be decoded before using this API.

Please note that only the first hash or address found in the request is used to find data. The rest of the message is ignored.

<table class="api">
<tr><td colspan="3">Indexer. sId=19 mId=<code>4</code></td></tr>
<tr><td>BitcoinP2PKHAddress: <code>2</code></td><td>bytearray</td><td>20-byte ripe160 hash</td></tr>
<tr><td>BitcoinScriptHashed: <code>9</code></td><td>sha256</td><td>sha256 (single) hash of the script-out (32 bytes)</td></tr>
</table>

#### FindAddressReply

The reply will list a combination of blockheight, offset in block (which together identifies one
single transaction) and output-index.

The triplets will be separated by a Separator item.

<table class="api">
<tr><td colspan="3">Indexer. sId=19 mId=<code>5</code></td></tr>
<tr><td>separator: <code>0</code></td><td>bool</td><td>indicator that next value will follow</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>natural-number</td><td>Height of the block in the chain.</td></tr>
<tr><td>OffsetInBlock: <code>8</code></td><td>natural-number</td><td>Key
useful for BlockChain::GetTransaction</td></tr>
<tr><td>output-index: <code>20</code></td><td>natural-number</td><td>(zero base) index of output in tx</td></tr>
</table>


#### FindSpentOutput

You can only ask about a spent txid when the spentOutputDB is enabled on the indexer.

This call takes a transaction-id and output-index and returns the
transaction that spent it.

In case the spender is not found, a blockheight of -1 is returned. See
FindSpentOutputReply

<table class="api">
<tr><td colspan="3">Indexer. sId=19 mId=<code>6</code></td></tr>
<tr><td>TxId: <code>4</code></td><td>bytearray</td><td>32-byte transaction hash</td></tr>
<tr><td>output-index: <code>20</code></td><td>natural-number</td><td>(zero base) index of output in tx</td></tr>
</table>


#### FindSpentOutputReply

You can only ask about a spent txid when the spentOutputDB is enabled on the indexer.

In case the spender is not found, a blockheight of -1 is returned. See
FindSpentOutputReply

<table class="api">
<tr><td colspan="3">Indexer. sId=19 mId=<code>7</code></td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>natural-number</td><td>Height of the block in the chain.</td></tr>
<tr><td>OffsetInBlock: <code>8</code></td><td>natural-number</td><td>Key
</table>
