+++
title = "Service/Live Transactions"
toc = true
weight = 2
parent = "api"
+++

### Live Transaction Service

Whereas most of the APIs are about historical (unchanging) records, this service
provides several APIs for live data, data that can change as time moves on.

Transactions in Bitcoin Cash are collected in a mempool before they are
mined in a block, this service allows you to fetch transactions from that
pool (and recently mined blocks) and to submit new transactions you want to
submit for mining.

Transactions that have been mined create outputs that hold money which can be
spent by the owner of that money. This information is collected in the
unspent-transaction-outputs database (in short UTXO DB). This service provides
querying features for the UTXO.

This service uses service ID `2`.

### Messages

#### GetTransaction

This fetches a transaction, returning an error (see
[hub](../hub/#commandfailed)) message if it is not found.

<table class="api">
<tr><td colspan="3">Hub / API. sId=2 mId=<code>0</code></td></tr>
<tr><td>TxId: <code>4</code></td><td>bytearray</td><td>32-byte transaction-id</td></tr>
</table>

#### GetTransactionReply

When a transaction was found it is returned using this reply message.

<table class="api">
<tr><td colspan="3">Hub / API. sId=2 mId=<code>1</code></td></tr>
<tr><td>BytesData: <code>1</code></td><td>bytearray</td><td>The raw transaction</td></tr>
</table>

#### SendTransaction

Offer to the network this message. The node will fully validate (check if
it is well formatted, fully complete and spending existing money) before it
will continue to share the message with the rest of the Bitcoin Cash
network.

If the transaction could not be accepted for any reason, we reply with a
[command failed](../hub/#commandfailed) message from the Hub service.

<table class="api">
<tr><td colspan="3">Hub / API. sId=2 mId=<code>2</code></td></tr>
<tr><td>Transaction: <code>20</code></td><td>bytearray</td><td>The raw transaction</td></tr>
</table>

#### SendTransactionReply

To confirm sending of the transaction a reply is created with the txid
(sha256 hash) that has been accepted by the peer.

<table class="api">
<tr><td colspan="3">Hub / API. sId=2 mId=<code>3</code></td></tr>
<tr><td>BytesData: <code>1</code></td><td>bytearray</td><td>32-byte transaction Id</td></tr>
</table>

#### IsUnspent

**Since 2019.06**

The UTXO database can be queried using this method.

All queries have to be done based on the spending transaction ID (a sha256) combined
with the output index (base-zero). If the output has been spent (or never existed) you will
get a simple 'false' in your reply, if the output is unspent you will receive the block-height
and the offset-in-block pair which you can use to get the full transaction from the historical
record using GetTransaction method from the Blockchain service.

Also notice the GetUnspentOutput request below which gives you the actual output data
in one go.

<table class="api">
<tr><td colspan="3">Hub / API. sId=2 mId=<code>4</code></td></tr>
<tr><td>separator: <code>0</code></td><td>bool</td><td>Indicator for a next question</td></tr>
<tr><td>TxId: <code>4</code></td><td>bytearray</td><td>32-byte transaction hash</td></tr>
<tr><td>Output_Index <code>21</code></td><td>natural-number</td><td>the
index of the output.</td></tr>
</table>

In one message many requests can be stored, each separated by a Separator flag (which is a
single byte).

#### IsUnspentReply

**Since 2019.06**

For each request the Unspent bool will always be returned, and the BlockHeight
and Tx\_OffsetInBlock will be included only when the output was found in the UTXO.

The request could have multiple lookups, each separated by a Separator. The answer
will have the same number in the same order.

<table class="api">
<tr><td colspan="3">Hub / API. sId=2 mId=<code>5</code></td></tr>
<tr><td>separator: <code>0</code></td><td>bool</td><td>Indicator for a next question</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>number</td><td>Height of the block in the chain.</td></tr>
<tr><td>Tx_OffsetInBlock: <code>8</code></td><td>natural-number</td><td>Byte-offst in block</td></tr>
<tr><td>Output_Index <code>21</code></td><td>natural-number</td><td>the index of the output.</td></tr>
<tr><td>UnspentState: <code>22</code></td><td>boolean</td><td>True if unspent, false if not found in utxo.</td></tr>
</table>

#### GetUnspentOutput

**Since 2019.06**

This message is identical to the one in IsUnspent, except that it will result in a reply that
includes more data.

<table class="api">
<tr><td colspan="3">Hub / API. sId=2 mId=<code>6</code></td></tr>
<tr><td>TxId: <code>4</code></td><td>bytearray</td><td>32-byte transaction hash</td></tr>
<tr><td>Output_Index <code>21</code></td><td>natural-number</td><td>the
index of the output.</td></tr>
</table>

#### GetUnspentOutputReply

**Since 2019.06**

For each request the Unspent bool will always be returned, the other
values will only be included when the output was found in the UTXO.

The request could have multiple lookups, each separated by a Separator. The answer
will have the same number in the same order.

<table class="api">
<tr><td colspan="3">Hub / API. sId=2 mId=<code>7</code></td></tr>
<tr><td>UnspentState: <code>22</code></td><td>boolean</td><td>True if unspent, false if not found in utxo.</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>number</td><td>Height of the block in the chain.</td></tr>
<tr><td>OffsetInBlock: <code>8</code></td><td>natural-number</td><td>Byte-offst in block</td></tr>
<tr><td>Output_Index <code>21</code></td><td>natural-number</td><td>the index of the output.</td></tr>
<tr><td>Amount <code>6</code></td><td>positive-number</td><td>The
amount of satoshis this output holds.</td></tr>
<tr><td>OutputScript <code>23</code></td><td>bytearray</td><td>Full
outputscript</td></tr>
</table>

