+++
title = "Service/AddressMonitor"
toc= true
weight = 17
parent = "api"
+++

### Address Monitor Service

This service, with service-id `17` is a subscription service. A connected
peer can subscribe to a list of bitcoin-addresses and as long as the
connection stays open the Hub will monitor incoming transacions and blocks
for matches, which it will report by sending messages to the peer.

This service was introduced in Flowee release 2019.05

### Messages

#### Subscribe

We use a BitcoinScriptHashed tag which is a sha256, send as a 32-byte bytearray.
subscriptions and detection happens, not exactly on address, but on the entire
output-script. The entire output script is hashed giving a nice constant size
identifier we use instead of an address.

The API is not meant to be user-facing, as such things like address encodings,
cash-address etc are irrelevant to this API.

You need to convert the address to an actual script (typically starting with OP\_DUP)
and hash that in order to get to the BitcoinScriptHashed.

<table class="api">
<tr><td colspan="3">Hub / API. sId=17 mId=<code>0</code></td></tr>
<tr><td>BitcoinScriptHashed: <code>9</code></td><td>sha256</td><td>sha256 (single) hash of the script-out (32 bytes)</td></tr>
</table>

#### SubscribeReply

<table class="api">
<tr><td colspan="3">Hub / API. sId=17 mId=<code>1</code></td></tr>
<tr><td>Result: <code>7</code></td><td>positive-number</td><td>the
amount of addresses found in the subscribe message</td></tr>
<tr><td>ErrorMessage: <code>9</code></td><td>string</td><td>Human readable error
message</td></tr>
</table>

#### Unsubscribe

<table class="api">
<tr><td colspan="3">Hub / API. sId=17 mId=<code>2</code></td></tr>
<tr><td>BitcoinScriptHashed: <code>9</code></td><td>sha256</td><td>sha256 (single) hash of the script-out (32 bytes)</td></tr>
</table>

#### TransactionFound

These messages are initiated by the service and don't require a reply. The
service sends a message for each transaction that touches one of the
subscribed addresses. One message is sent to the client if accepted into
mempool, another when accepted in a block.

<table class="api">
<tr><td colspan="3">Hub / API. sId=17 mId=<code>3</code></td></tr>
<tr><td>TxId: <code>4</code></td><td>bytearray</td><td>32-byte sha256 hash</td></tr>
<tr><td>BitcoinScriptHashed: <code>9</code></td><td>sha256</td><td>sha256 (single) hash of the script-out (32 bytes)</td></tr>
<tr><td>Amount: <code>6</code></td><td>natural-number</td><td>Amount of
satoshi received</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>number</td><td>Height
of the block in the chain.</td></tr>
<tr><td>OffsetInBlock: <code>8</code></td><td>natural-number</td><td>Key
useful for GetTransaction (only when confirmed)</td></tr>
</table>

#### DoubleSpendFound

These messages are initiated by the service and don't require a reply. The
service sends a message for each double spend that touches one of the
subscribed addresses.

<table class="api">
<tr><td colspan="3">Hub / API. sId=17 mId=<code>4</code></td></tr>
<tr><td>TxId: <code>4</code></td><td>bytearray</td><td>32-byte sha256 hash</td></tr>
<tr><td>BitcoinScriptHashed: <code>9</code></td><td>sha256</td><td>sha256 (single) hash of the script-out (32 bytes)</td></tr>
<tr><td>Amount: <code>6</code></td><td>natural-number</td><td>Amount of
satoshi received</td></tr>
<tr><td>Full raw transaction: <code>1</code></td><td>bytearray</td>A full transaction<td></td></tr>
</table>
