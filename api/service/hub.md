+++
title = "Service/Hub Control"
toc= true
weight = 3
parent = "api"
+++

### HUB API Service

This service, with service-id `0`, is probably easiest to explain as it
being a meta service. The Hub has a set of standard services which are all
grouped under the concept "API" (Application Programmer Interface). This,
it should be stressed, does not cover the entire set of APIs in Flowee, or
even in the Hub.

The list of services that are owned by the Hub API is the following:

[API Service](/docs/api/service/hub/)
: `0`) A meta representing the Hubs API. If any command fails, you'll
get a message from this service.

[BlockChain Service](/docs/api/service/blockchain/)
: `1`) Query the blockchain-database.

[RawTransaction Service](/docs/api/service/rawtransaction/)
: `2`) Retrieve or send raw transaction data.

[Util Service](/docs/api/service/util/)
: `3`) Generic utilities provided by the hub. For instance 'CreateAddress'

[Test Service](/docs/api/service/regtest/)
: `4`) Local testing service. This is for a Hub running on a testing chain
that is not typically shared with others and as such is possible to stay
completely local and mining is always instant.


Please notice that a new connection to the Hub requires you to send a valid
message to the APIs services within a couple of seconds or you will get
disconnected. We suggest asking for the [version](#version).

### Messages

#### Version

<table class="api">
<tr><td colspan="3">Hub / API. sId=0 mId=<code>0</code></td></tr>
</table>

#### VersionReply

The version string is essentially the same between the API and the on-the-net string.
An example of this is "Flowee:1 (2019-4.1)".

In more readable terms This is: {Name}:{API version} ({year}-{release}.{patch})

<table class="api">
<tr><td colspan="3">Hub / API. sId=0 mId=<code>1</code></td></tr>
<tr><td>VersionString: <code>1</code></td><td>string</td><td>Version string</td></tr>
</table>

#### CommandFailed

For any of the APIs (as listed at the top of this page) errors are
registered and handled by this service. So a call to the Blockchain service
will generate a reply of the following message if there was a failure.

<table class="api">
<tr><td colspan="3">Hub / API. sId=0 mId=<code>2</code></td></tr>
<tr><td>FailedReason: <code>20</code></td><td>string</td><td>Error message</td></tr>
<tr><td>FailedCommandServiceId: <code>21</code></td><td>natural-number</td><td>Service ID of the command that failed</td></tr>
<tr><td>FailedCommandId: <code>22</code></td><td>natural-number</td><td>Message Id of the command that failed</td></tr>
</table>
