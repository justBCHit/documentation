+++
title = "Example Message"
weight = 20
parent = "api"
+++

### Example Message: Version (+ reply)

The version message is a request, the versionReply is the reply.

Requesting a version is essentially as simple as sending an empty message (no body) to the right service and with the right messageId.

<table class="api">
<tr><td colspan="3">MesageSize: 7</td><td>0x0700</td></tr>
<tr><td>ServiceId: 1</td><td>positive-int</td><td>APIservice: 0</td><td>0x0800</td></tr>
<tr><td>MessageId: 1</td><td>positive-int</td><td>Meta::Version: 0</td><td>0x1000</td></tr>
<tr><td>End-Header: 0</td><td colspan="2">boolean-true</td><td>0x04</td></tr>
</table>

Answer is equally simple, except that it has an actual value with a string representation
of the version.

<table class="api">
<tr><td colspan="3" width=75%>MesageSize: 28</td><td>0x1c00</td></tr>
<tr><td>ServiceId: 1</td><td>positive-int</td><td>APIservice: 0</td><td>0x0800</td></tr>
<tr><td>MessageId: 1</td><td>positive-int</td><td>Meta::VersionReply: 1</td><td>0x1001</td></tr>
<tr><td>End-Header: 0</td><td colspan="2">boolean-true</td><td>0x04</td></tr>
<tr><td>Meta::GenericBytes: 0</td><td colspan="2">string: "Flowee:1 (2019-4.1)"</td><td>0x0A13466C6F7765653A31 2028323031392D342E3129</td></tr>
</table>

