+++
title = "Configuring the Hub"
#toc= true
parent = "hub"
weight = 20
+++

After you picked your setup in [plan your
deployment](../plan-your-deployment), the next step is configuring your
Hub. This is typically quite simple as the default settings will be good
enough for most.

Even so, the full details of the configuration of the Hub are shown on this
page. Please also notice the configuration of the [logging
subsystem](../log-config) which is separate.


### General notes

All config options on this page are for the Hub, they can be passed on the
command line or in the systemd service file. In that case you should
precede them with a dash. Like so:
```
hub -connect=hostname
```
Also shown in this example is that arguments to the options are separated
by an equals sign. No spaces are allowed to separate the option and the
arguments. Use quotes if needed.

Options can also be stored in the config file, which makes them look like
this:

```
apicookiefile=/etc/flowee/api-cookie
```

Notice that the leading dash is removed here. Arguments are passed
similarly. One option per line.

version
: Print version and exit

#### Configuration location options:

conf=\<file\>
: Specify configuration file (default: flowee.conf). [Note about dirs](#dirs-search).


datadir=\<dir\>
: Specify data directory

#### General options:

checkblocks=\<n>
: How many blocks to check at startup (default: 5, 0 = all).  
This feature is only really relevant to check the database consistency.
Make sure we didn't have any disk-corruption.

checklevel=\<n>
: How thorough the block verification of -checkblocks is (0-4, default: 3).  
Level 3 and 4 also check the UTXO.

daemon
: Run in the background as a daemon (returning to the commandline
immediately) and accept commands from hub-cli

maxorphantx=\<n>
: Keep at most \<n> unconnectable transactions in memory (default: 5000).
An unconnectable transaction is one that we don't know if it is spending
any actual money and as such we don't know if they are valid. We might get
a transaction later that connects an orphaned transaction to an actual
chain of transactions.

maxmempool=\<n>
: Keep the transaction memory pool below \<n> megabytes (default: 300)

mempoolexpiry=\<n>
: Do not keep transactions in the mempool longer than \<n> hours (default: 72)

pid=\<file>
: Specify pid file (default: floweethehub.pid)

reindex
: Rebuild block chain index from current blk000??.dat files on startup.


blockdatadir=\<dir>
: List a fallback directory to find blocks/blk\* files.  
Block files (blk00000.dat etc) are written in the datadir and when the Hub
wants to read the block again it tries first in the datadir again. Using
this option you can pass in a series of fallback locations to look for
blocks. You can use this option multiple times, the first one found is the
first fallback, in sequence.

#### Api server options:

The API server is the main protocol that different Flowee components use to
connect to each other.

apicookiefile=\<loc>
: Location of the apiserver auth cookie (default: 'api-cookie').  [Note about dirs](#dirs-search).  
The cookie is auto-generated should the file not exist yet. It is simply
read all consecutive times.

apilisten=\<addr>
: Bind to given address to listen for api server connections. Use
[host]:port notation for IPv6. This option can be specified multiple times
(default 127.0.0.1:1235 and [::1]:1235).  
Testnet is at port 11235 by default.  
The default only listens on loopback. Which means it is not visible from
any other machine. You can bind to multiple addresses, if your machine has
more than one network interface.

#### Connection options:

These options are solely for connecting to the Bitcoin network. Which is
typically about the public internet. Although these options may allow you
to chain your nodes on a private network.

addnode=\<ip>
: Add a bitcoin-node to connect to and attempt to keep the connection open

banscore=\<n>
: Threshold for disconnecting misbehaving peers (default: 100).  
A remote node that doesn't speak the Bitcoin protocol correctly is given a
punishment score for each infraction. Should the punishments add up to the
banscore, the node is disconnected and banned from connected again for some
time.

bantime=\<n>
: Number of seconds to keep misbehaving peers from reconnecting (default:
86400 = 24 hours)

bind=\<addr>
: Bind to given address and always listen on it. Use [host]:port notation
for IPv6.  
Being bound to a certain interface on the host machine allows other nodes
to connect to you. Do this, and maybe port-forward your node to the
external internet if you want SPV wallets and other users to be able to use
your node for general, anonymous, requests.

connect=\<ip>
: Connect only to the specified bitcoin-node(s).  Notice that you can use
connect multiple times, to connect to multiple nodes. It is important to
realize that if you use connect the node will not try to actively find
other connections. Use 'addnode' if you don't want to disable that.

discover
: Discover own IP addresses (default: true when listening and no -externalip or -proxy)

dns
: Allow DNS lookups for -addnode, -seednode and -connect (default: 1)

dnsseed
: Query for peer addresses via DNS lookup, if low on addresses (default: true unless -connect)

externalip=\<ip>
: Specify your own public address, useful if you don't have a upnp enabled
router.

forcednsseed
: Always query for peer addresses via DNS lookup (default: 0). If we have
enough remote nodes in our peer database, there is no need for DNS lookup.

listen
: Accept connections from outside (default: true if no -proxy or -connect).
Other than actually listening you need to make your node visible. If you
want to allow others to connect to your node from the public internet, you
likely need to enable port-forwarding (ask your sys-admin how).

listenonion
: Automatically create Tor hidden service (default: 0)

maxconnections=\<n>
: Maintain at most \<n> connections to peers (default: 125)

min-thin-peers=\<n>
: Maintain at minimum \<n> connections to thin-capable peers (default: 2).
The 'xthin-blocks' protocol standard allows much smaller blocks to be sent
between peers.

maxreceivebuffer=\<n>
: Maximum per-connection receive buffer, \<n>\*1000 bytes (default: 5000)

maxsendbuffer=\<n>
: Maximum per-connection send buffer, \<n>\*1000 bytes (default: 1000)

onion=\<ip:port>
: Use separate SOCKS5 proxy to reach peers via Tor hidden services (default: -proxy)

onlynet=\<net>
: Only connect to nodes in network \<net> (ipv4, ipv6 or onion). This is
useful to limit if your node is on a network unable to reach the internet
on ipv6, for instance. If properly set, the hub will find new hosts faster.

port=\<port>
: Listen for connections on \<port> (default: 8333 or testnet: 18333).

proxy=\<ip:port>
: Connect through SOCKS5 proxy

proxyrandomize
: Randomize credentials for every proxy connection. This enables Tor stream isolation (default: 1)

seednode=\<ip>
: Connect to a node to retrieve peer addresses, and disconnect

timeout=\<n>
: Specify connection timeout in milliseconds (minimum: 1, default: 5000)

torcontrol=\<ip>:\<port>
: Tor control port to use if onion listening enabled (default: 127.0.0.1:9051)

torpassword=\<pass>
: Tor control port password (default: empty)

maxuploadtarget=\<n>
: Tries to keep outbound traffic under the given target (in MiB per 24h), 0
= no limit (default: 0). This is great if the hub is reachable from the
internet and you are on a metered connection. Otherwise you can just leave
it turned off.

#### Debugging/Testing options:

uacomment=\<comment>
: Append comment to the user agent string

gen
: Generate coins (default: 0).  Please note that 'generate' or 'mining' on
a CPU is not going to work, you need specialized hardware for this. There
is a reason this option is in the debug section.

genproclimit=\<n>
: Set the number of threads for coin generation if enabled (-1 = all cores, default: 1)

gencoinbase=\<pubkey>
: When generating coins a coinbase has to be provided in the form of a public key

logips
: Include IP addresses in debug output (default: 0)

printtoconsole
: Send trace/debug info to console instead of debug.log file

shrinkdebugfile
: Shrink debug.log file on client startup (default: true when no -debug)

#### Chain selection options:

testnet
: Use the test chain

#### Node relay options:

blocksizeacceptlimit=\<n>
: This node will not accept blocks larger than this limit. Unit is in MB (default: 32.0)

bytespersigop=\<n>
: Minimum bytes per sigop in transactions we relay and mine (default: 20)

datacarrier
: Relay and mine data carrier transactions (default: 1)

datacarriersize=\<n>
: Maximum size of data in data carrier transactions we relay and mine (default: 223)

expeditedblock=\<host>
: Request expedited blocks from this host whenever we are connected to it

maxexpeditedblockrecipients=\<n>
: The maximum number of nodes this node will forward expedited blocks to

maxexpeditedtxrecipients=\<n>
: The maximum number of nodes this node will forward expedited transactions to

minrelaytxfee=\<amt>
: Fees (in BCH/kB) smaller than this are considered zero fee for relaying, mining and transaction creation (default: 0.00001)

use-thinblocks
: Enable thin blocks to speed up the relay of blocks (default: true)

#### Block creation options:

blockminsize=\<n>
: Set minimum block size in bytes (default: 0)

blockmaxsize=\<n>
: Set maximum block size in bytes (default: 8000000)

blockprioritysize=\<n>
: Set maximum size of high-priority/low-fee transactions in bytes (default: 100000)

#### RPC server options:

server
: Accept command line and JSON-RPC commands

rest
: Accept public REST requests (default: 0)

rpcbind=\<addr>
: Bind to given address to listen for JSON-RPC connections. Use [host]:port notation for IPv6. This option can be specified multiple times (default: bind to all interfaces)

rpccookiefile=\<loc>
: Location of the auth cookie (default: .cookie). [Note about dirs](#dirs-search).


rpcport=\<port>
: Listen for JSON-RPC connections on \<port> (default: 8332 or testnet: 18332)

rpcallowip=\<ip>
: Allow JSON-RPC connections from specified source. Valid for \<ip> are a single IP (e.g. 1.2.3.4), a network/netmask (e.g. 1.2.3.4/255.255.255.0) or a network/CIDR (e.g. 1.2.3.4/24). This option can be specified multiple times

rpcthreads=\<n>
: Set the number of threads to service RPC calls (default: 4)

--------------

#### Dirs-search

Note that files without full paths are searched for in the following order;

1. Inside the data directory.
2. In the XDG data dir ($HOME/.local/share/flowee/)
3. In the XDG config dir ($HOME/.config/flowee/)
4. In the users homedir ($HOME/)

