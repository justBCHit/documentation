+++
title = "Log Configuration"
toc= true
parent = "hub"
weight = 30
+++

### Setting up system logging

Flowee the Hub ships its own logging system which has been created to
address the needs of the software developers as well as the operators
and sys-admins. The way to support all these groups is to make the system
quite flexible and configurable.

Flowee currently ships with two loggers, a command-line output logger as
well as a file-logger.

To configure which logger will be used and what content will actually be
logged Flowee the Hub will try to find and parse a config file named
`logs.conf`.

Users running the software can find the config file in either the
datadir (by default $HOME/.local/share/flowee) or the config dir
($HOME/.config/flowee). For setups where a specific user is created to run
the software you will likely just want to place the config file directly in
the homedir of the user.


### channels

There are currently two types of channels `console` and `file`. Each can
receive a number of different options.

```
channel console
    option timestamp

channel file
    option timestamp time millisecond
```

The usage of the 'channel' keyword implies you enable that channel, if you
have no channels your logs will go nowhere!

You can configure each channel with a number of options. In the example the
console will not have any timestamps, while the file output will list
time without dates, showing millisecond if needed.

The options are:

option timestamp
: Configure timestamps. The arguments are any combination of `date`, `time`
  and `millisecond`. No argument means you turn off timestamps.  
Default is `time millisecond` for the console channel, the file channel
defaults to `date time millisecond`.

option path [filepath]
: This allows configuration of the file that the log goes to. Only used by
  the file channel.  
Default: [datadir]/hub.log

option linenumber true
: Prefix output with the linenumber of the source file. This only works for
  debug builds as other builds do not embed this info.  
  Default: off

option methodname true
: Prefix output with the methodname. This only works for debug builds as
  other builds do not embed this info.  
  Default: true

option filename true
: Prefix output with the filename of the source file. This only works for
  debug builds as other builds do not embed this info.  
  Default: false


### Log sections and levels

The rest of the config file is reserved for log-levels.

```
ALL quiet
1001 silent
```
This is a rather simple example showing the idea. The software is divided
into sections and you can give each its own log-level. This allows for
rather fine level of control.
In this example we set the global level to 'quiet', which ignores the
details, warnings and informative messages and we then continue to set
group `1001` to silent, which ignores even more log messages.

Log sections are numbered and fully [documented in their own page](../log-sections/).  
Log levels are named, full details [in their own page](../../devel/loglevel-guidelines/).

For convenience of configuration the sections are sub-divided into groups. Each group being a multiple of 1000.
```
2000 debug #networking
2001 silent
```

In the above example we set the group networking to show all messages. Because this is a group this will affect all network sections. 2000 - 2999. You can still override individual sections!

### Reloading of config file

On Unix systems you can change the logs.conf file and then send the unix signal `SIGHUP` to trigger the re-parsing of the configuration file.

This signal will also cause the log file to be closed and re-opened, which is useful for log-rotation.
