+++
title = "Log-Sections"
bref = "For operators and developers"
parent = "hub"
weight = 410
+++

### Log-Sections

In Flowee we have a logging framework which is useful for multiple purposes
from developer (debugging) information to fault-finding and general
operational monitoring.

**Log Sections**

The software developer writes code to log events and he has to choose a log-section. Similarly, the operator will choose which log-sections they are interested in.

Software with this line `logCritical(Log::RPC) << "event";` will only log
if the operator had selected an appropriate log-level for the RPC
log-section.


| Section | number | comment |
|---|---|---|
Global | 0 | Group. For developers only.
Bitcoin | 1000 | Group. |
BlockValidation | 1001 | Validation of blocks and updating the blockchain|
TxValidation | 1002 | Transaction validation |
Bench | 1003 | Benchmarking group|
Unused | 1004 | |
Mining | 1005 | |
Networking | 2000 | Group. |
Net | 2001 | The peer-2-peer networking
Addrman | 2002 | Address-manager. To remember possible peers.
Proxy | 2003 |
NWM | 2004 | Network-Manager
Tor | 2005
ThinBlocks | 2006 | x-thin-blocks processing.
ExpeditedBlocks | 2007
RPC | 2100 | Group. Most of this is legacy
LibEvent | 2101 | for the http server which serves the RPC requests.
HTTP | 2102
ZMQ | 2103
ApiServer | 2500 | The Flowee specific APIs|
MonitorService | 2501 | The network service for monitoring. ([API](/docs/api/service/addressmonitor/))
DB | 3000 | Group. Internal databases
Coindb | 3001 | UTXO database
Internals | 4000 | Group.
Mempool | 4001 |
MempoolRej | 4002 | Rejected mempoool entries
Random | 4003 | Random number generator
Wallet | 5000 | Group. Legacy wallet.
SelectCoins | 5001 |
FeeEstimation | 5002 |
QtGui | 6000 | Group, the qDebug output goes here.
POS | 7000 | Group | For the point of sale product


See also [log-level guidelines](/docs/devel/loglevel-guidelines/) & [Configuration](../log-config/).
