+++
title = "search()"
weight = 20
toc = true
parent = "floweejs"
+++

This chapter describes the functions and properties related to the search() family of features.

Where the name `Flowee` is used, this is the variable which results from a call similar to:

`var Flowee = require('bindings')('flowee')`

### `Flowee.search( {} )`

The search function takes an object literal that can optionally have various functions and properties. Functions provided are called by the search engine when certain types of data have been found. For instance, when a new transaction has been found the user function `onTxAdded()` is called with the transaction as a property.

The search object takes as input so called "jobs". There can be many search-jobs that together make one search and the results are gathered in lists of transactions and block-headers.

The search will finish when all jobs have been completed and then the `onFinished()` function is called for the user to parse the results.

###### Parameters

* Request Object  
an object literal that will become the search object. See [Request Object](#request-object) below.

###### Return-value

This function returns a promise. Notice that the promise in some cases requires at least NodeJS v12.6

### `Flowee.search( [] )`

This is an overloaded version of the search() above, this version takes only the 'jobs' array as an argument.

###### Parameters

* `jobs`  
An array of job objects.

###### Return-value

This function returns a promise. Notice that in this case the promise requires at least NodeJS v12.6

### Flowee Include enum

This enum is used in the `Job.txFilter` property. It is used in jobs that fetch transactions to
determine which parts of a transaction to download from the server and place in the result set.

* `Flowee.IncludeOffsetInBlock`  
Fetch the offset, in bytes, in a block of the transaction. Flowee the Hub
regards a blockheight with this offset as a key for a specific transaction
in the chain.

* `Flowee.IncludeInputs`  
Include all inputs for the transaction.

* `Flowee.IncludeTxId`  
Include the transaction-id hash.

* `Flowee.IncludeFullTxData`  
This includes a byte-array of the raw transaction data.

* `Flowee.IncludeOutputs`  
Include all properties of outputs. This is the same as setting the next 2
properties.

* `Flowee.IncludeOutputAmounts`  
Include, for each output, the amount paid to it.  

* `Flowee.IncludeOutputScripts`  
Include, for each output, the full bitcoin-script.

* `Flowee.IncludeOutputAddresses`  
Instead of a script, include the p2pkh address we paid to, if any. Notice
that this only works for pay-to-public-key-hash (P2PKH) type of outputs.
Behaviour is undefined for other types.

### Flowee.Job type enum

* `Flowee.Job.FetchTx`  
Fetch a single transaction. The argument should be one of the below two
options. In the case of providing a txid the framework will auto-replace
this job with a LookupTxById and a new FetchTx.  
1) value: blockheight, value2: offset-in-block.  
2) value: TXID

* `Flowee.Job.FetchBlockHeader`  
Fetches a list of meta-data about a block. The value should either be a
blockheight or the block-hash.

* `Flowee.Job.FetchBlockOfTx`  
Fetches a block of transactions based on the blockheight or the block-hash.

* `Flowee.Job.LookupTxById`  
Use the Indexer to resolve a transaction-id hash to the key that the Hub
uses. Returns the blockheight and offset-in-block.

* `Flowee.Job.LookupByAddress`  
Fetches which transactions deposited funds in the specified address. The
value here can be a legacy address, a bitcoin cash address or a sha256 hash
of the output script.

* `Flowee.Job.LookupSpentTx`  
Fetches a specific output of a transaction which spent it (if any). This is
effectively the mirror functionality of the UTXO lookups. The "value"
property should be a transaction-id and the value2 property should be set
to the output of the creation transaction.

* `Flowee.Job.FetchUTXOUnspent`  
Fetches a simple yes/no if a transaction output is unspent. Is the faster
and cheaper version of the following option. The "value" property should
be a transaction-id and the value2 property should be set to the output of
the creation transaction.

* `Flowee.Job.FetchUTXODetails`  
This is almost the same as the FetchUTXOUnspent, except that this one
returns also the amount that was deposited in this output as well as the
script which locks it.

### Request Object

The request object is what you pass into the search, and this object is the 'this' in the
functions, including the `onFinished()` function.

Example:

{{< highlight js >}}
Flowee.search({
  jobs: [
     {  // Fetch this transaction
        value: txid,
        type: Flowee.Job.FetchTx,
        txFilter: [
            Flowee.IncludeOutputAmounts, Flowee.IncludeTxId
        ]
     }
  ],

  onTxAdded: function(transaction) {
    this.addJob({
        type: Flowee.Job.FetchBlockHeader,
        value: transaction.blockHeight
    });
  },

  onFinished: function(unFinishedJobCount) { /* process results here */ }
{{< / highlight >}}

##### Properties

The request object has these properties. All are optional.

* `jobs`  
This is an array of jobs. See [Job](#job) for details on this class.

* `onAddressUsedInOutput`  
Allows users to set a function pointer for a callback. (optional)  
This function is called when a job of type `Flowee.LookupByAddress` has returned with results.  
`onAddressUsedInOutput: function(blockHeight, offsetInBlock, outIndex) {}`

* `onTxAdded`  
Allows users to set a function pointer for a callback. (optional)  
This function is called when a job of type `Flowee.FetchTx` has returned with results.  
`onTxAdded: function(transaction) {}`

* `onFinished`  
Allows users to set a function pointer for a callback. (optional)  
This function is called when all jobs have completed. The argument
'unFinishedJobCount' is the amount of jobs that the search didn't manage to
start. Typically due to missing properties.  
`onFinished: function(unFinishedJobCount) {}`

* `onTxIdResolved`  
Allows users to set a function pointer for a callback. (optional)  
This function is called when a job of type `Flowee.LookupTxById` has returned with results.  
`onTxIdResolved: function(jobId, blockHeight, offsetInBlock) {}`


* `onSpentOutputResolved`  
Allows users to set a function pointer for a callback. (optional)  
This function is called when a job of type `Flowee.LookupSpentTx` has returned with results.  
`onSpentOutputResolved: function(jobId, blockHeight, offsetInBlock) {}`

* `onUtxoLookup`  
Allows users to set a function pointer for a callback. (optional)  
This function is called when a job of type `Flowee.FetchUTXOUnspent` or `Flowee.FetchUTXODetails` has returned with results.  
`onUtxoLookup: function(utxoEntry) {}`

* `binaryHashes`  
Should the user set this to true the hashes (txid, previousTxid) are created as bytearrays instead of hex-strings.

* `transactions`  
An array of [Transaction](#transaction) objects, as populated by the search
engine containing the actually found transactions. Consider this read-only.

* `block$height`  
Each block-header that was fetched (with Flowee.FetchBlockHeader) is placed
in a property with the format `block100` for the
[header](#blockheader) of the block at height 100.

##### Functions

* `addJob(job)`  
append a job to the jobs array. Which, if all values are provided, will be
executed.

### Transaction class

A transaction object is created by search and stored in an array on the Request Object under the property `transactions`. Notice that based on the job.txFilter option only some of these properties may be filled in.

* `jobId`  
The index in the request objects `jobs` array that was responsible for fetching this transaction.
* `txid`  
The transaction id (sha256 hash)
* `blockHeight`  
The blockheight is the index of the block mined. Genesis was 11 years ago mined at block height 0, there is on average one new block added to this chain every 10 minutes.
* `offsetInBlock`  
The byte-offset in the block that this transaction is located.
* `isCoinbase`  
If this is a coinbase transaction, which creates new money as a block-reward, then this is set to true.
* `inputs`  
An array of inputs.
* `input.outputIndex`  
An index spends another transaction's output, this indicates the output-index it spends.
* `input.previousTxid`  
An index spends another transaction's output, this indicates which transaction it spends.
* `input.script`  
The input script contains the signatures and other proof that unlocks the output script it spends.
* `input.script-array`  
This is the inputScript, but in array format (raw).
* `outputs`  
An array of outputs.
* `output.index`  
The index of the output.
* `output.amount`  
The amount of money that this output spends. In satoshis.
100&nbsp;000&nbsp;000 satoshis go in one Bitcoin.
* `output.script`  
The output script is the locking script for this output.
* `output.script-array`  
This is the same outputscript, but in array format (raw).
* `fullTxData`  
An array of the raw transaction data.

### Blockheader

The Blockheader object is stored in `block$height` named properties, on the
request-object. You'd write:
```
const header = result[`block${tx.blockHeight}`];
console.log(header.hash);
```

* `height`  
The block height is the index of the block mined. Genesis was 11 years ago mined at block height 0, there is on average one new block added to this chain every 10 minutes.
* `confirmations`  
The amount of blocks mined after this block on the blockchain, more means better security. Copied at the time of running the search.
* `hash`  
The block-hash.
* `version`  
Block version.
* `time`  
The timestamp of this block that the miner added when they created it (in seconds since epoch).
* `mediantime`  
The middle time of the last 11 blocks. Which is just the block time of 6 blocks ago (in seconds since epoch).
* `nonce`  
The random number added by the miner as part of the mining process.
* `bits`  
The difficulty, represented in bits.
* `difficulty`  
The difficulty represented in floating point.
* `merkleroot`  
This blocks merkle-root.

### Job

The Job object is created by users and sometimes by the framework and
typically stored on the `jobs` array which is part of the request-object.

* `type`  
This has to be filled in and come from the [Job-type enum](#flowee-job-type-enum).
* `value`  
This contains the main value that is the input of the job. Job-type dependent.
* `value2`  
This (optionally) contains another value, specific for the job-type.
* `value3`  
This (optionally) contains another value, specific for the job-type.
* `txFilter`  
If the job is a type that fetches transactions then this should be an array of items from the [Include enum](#flowee-include-enum). 
